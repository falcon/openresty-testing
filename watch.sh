#!/usr/bin/env bash
basepath=$(cd `dirname $0`; pwd)
nginx -p $basepath 2>/dev/null
cd $basepath
nodemon="./node_modules/nodemon/bin/nodemon.js"
$nodemon -e conf,sh --exec "nginx -s reload -p" $basepath  --watch ./conf >./nodemon.log
#$nodemon --exec "/usr/bin/env bash" ./git-pull.sh --watch ./history/pull.lock >./history/nodemon.log 2>&1
